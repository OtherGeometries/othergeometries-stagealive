# The Stage is (a)Live


Short synopsis: a web based installation that stages the interactions between algorithmic dancers that set into motion a myriad of audio pieces and visual elements. This work is part of an on-going project called Choreographies of the [Circle & Other Geometries](http://www.geometries.xyz/index.html), a research on socio-technical protocols for collaborative audio-visual live coding and a corresponding peer-to-peer environment programmed in JavaScript.
